<?php

namespace Tests\Feature;
namespace App\Http\Controllers;
use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class ProjectControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
        

//Endpoint Create Record
  public function test_project_added()
  {
      $this->withoutExceptionHandling();
      $response = $this->post('/api/projects', [
          'name' => 'Project ONE',
          'detail' => 'This is Project One.'
      ]);
      $response->assertStatus(200);
      $this->assertTrue(count(Project::all()) > 1);
  }

   //Endpoint GET All Record
   public function testgetall() {
    $this->json('get', 'api/projects')
        ->assertStatus(Response::HTTP_OK)
        ->assertJsonStructure(
        [
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'detail',                        
                    'created_at',
                    'updated_at',                        
                ]
            ]
        ]
        );
}

  //Endpoint Update Record 
  public function test_project_updated()
  {
      $this->withoutExceptionHandling();
      $response = $this->put('/api/projects/1', [          
          'name' => 'Project One Updated',
          'detail' => 'This is Project One.'
      ]);
      $response->assertStatus(200);
      $this->assertTrue(count(Project::all()) > 1);
  }
 

  
}
