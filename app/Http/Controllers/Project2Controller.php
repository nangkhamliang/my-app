<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class Project2Controller extends Controller
{
    public function index()
    {
        $project = Project::all();
              
        return response()->json([
            'success' => true,
            'data' => $project
        ]);
    }

    public function store(Request $request) {

        $input = $request->all();        
        $validator = Validator::make($input, [
        'name' => 'required',
        'detail' => 'required'        
        ]);

        if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());       
        }

        $project = Project::create($input);
        
        if (!$project) {
            return response()->json([
                'success' => false,
                'message' => 'Project could not be created!'
            ], 400);
        }
        if ($project)
        {
        return response()->json([
        "success" => true,
        "message" => "Project created successfully.",
        "data" => $project
        ]);
        }
        
    }

    public function show ($id) {
        
        $project =Project::find($id);
 
        if (!$project) {
            return response()->json([
                'success' => false,
                'message' => 'Project is not available! '
            ], 400);
        }
 
        return response()->json([
            'success' => true,
            'data' => $project->toArray()
        ], 400);
    }    

    public function update(Request $request, $id) {

        $project = Project::find($id);
 
        if (!$project) {
            return response()->json([
                'success' => false,
                'message' => 'Project could not be found!'
            ], 400);
        }
 
        $updated = $project->fill($request->all())->save();
 
        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Project could not be updated!'
            ], 500);
    }

      

    public function destroy() {

        //
    }
 
   
}
