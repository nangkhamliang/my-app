<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index()
    {
        $project = auth()->user()->project;
              
        return response()->json([
            'success' => true,
            'data' => $project
        ]);
    }
 
    public function show($id)
    {
        $project = auth()->user()->project()->find($id);
 
        if (!$project) {
            return response()->json([
                'success' => false,
                'message' => 'Project is not available! '
            ], 400);
        }
 
        return response()->json([
            'success' => true,
            'data' => $project->toArray()
        ], 400);
    }
 
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'detail' => 'required'
        ]);
 
        $project = new Project();
        $project->name = $request->name;
        $project->detail = $request->detail;
 
        if (auth()->user()->project()->save($project))
            return response()->json([
                'success' => true,
                'data' => $project->toArray()
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Project could not be added!'
            ], 500);
    }
 
    public function update(Request $request, $id)
    {
        $project = auth()->user()->project()->find($id);
 
        if (!$project) {
            return response()->json([
                'success' => false,
                'message' => 'Project could not be found!'
            ], 400);
        }
 
        $updated = $project->fill($request->all())->save();
 
        if ($updated)
            return response()->json([
                'success' => true
            ]);
        else
            return response()->json([
                'success' => false,
                'message' => 'Project could not be updated!'
            ], 500);
    }
 
    public function destroy($id)
    {
        $project = auth()->user()->project()->find($id);
 
        if (!$project) {
            return response()->json([
                'success' => false,
                'message' => 'Project could not be found!'
            ], 400);
        }
 
        if ($project->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Project could not be deleted!'
            ], 500);
        }
    }
}
